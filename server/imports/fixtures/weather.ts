import {WeatherCollection} from '../../../both/collections/weather.collection';
import {Weather} from '../../../both/models/weather.model';


export function loadWeather() {
    if (WeatherCollection.find().cursor.count() === 0) {
        const parties: Weather[] = [
            {month_id: 1, value: 13},
            {month_id: 2, value: 14},
            {month_id: 3, value: 16},
            {month_id: 4, value: 18},
            {month_id: 5, value: 21},
            {month_id: 6, value: 25},
            {month_id: 7, value: 28},
            {month_id: 8, value: 28},
            {month_id: 9, value: 25},
            {month_id: 10, value: 21},
            {month_id: 11, value: 16},
            {month_id: 12, value: 13}
        ];

        parties.forEach((weather: Weather) => WeatherCollection.insert(weather));
    }
}