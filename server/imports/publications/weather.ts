import {Meteor} from 'meteor/meteor';
import {WeatherCollection} from "../../../both/collections/weather.collection";

// console.log('publish weather', Meteor.userId());
Meteor.publish('weather', function () {
    if (this.userId) {
        console.log('user', this.userId);
        return WeatherCollection.collection.find({});
    }
});