import {Meteor} from 'meteor/meteor';

import {loadWeather} from './imports/fixtures/weather';
import './imports/publications/weather';

Meteor.startup(() => {
    console.log("Start Meteor");
    loadWeather();
});