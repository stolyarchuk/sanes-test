import {MongoObservable} from 'meteor-rxjs';
import {Weather} from '../models/weather.model';

export const WeatherCollection = new MongoObservable.Collection<Weather>('weather');
