import {CollectionObject} from './collection-object.model';

export interface Weather extends CollectionObject {
    month_id: number;
    value: number;
}
