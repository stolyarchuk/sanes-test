import {Route}      from '@angular/router';

import {SignupComponent} from "./auth/signup.component";
import {LoginComponent} from "./auth/login.component";
import {AdminChartComponent} from "./admin/charts.component";

export const routes: Route[] = [
    {path: '', component: LoginComponent},
    {path: 'signup', component: SignupComponent},
    {path: 'admin', component: AdminChartComponent, canActivate: ['canActivateForLoggedIn']}
];

export const ROUTES_PROVIDERS = [{
    provide: 'canActivateForLoggedIn',
    useValue: () => {
        return !!Meteor.userId();
    }
}];