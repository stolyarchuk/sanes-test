import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {MeteorObservable} from "meteor-rxjs";

import {Weather} from "../../../../both/models/weather.model";
import {WeatherCollection} from "../../../../both/collections/weather.collection";

import template from './charts.component.html';

@Component({
    selector: 'admin-chart',
    template
})
export class AdminChartComponent implements OnInit, OnDestroy {
    weather: Weather[];
    private weatherSub: Subscription;

    constructor(private router: Router, private zone: NgZone) {
        console.log('[AdminChartComponent:constructor]');
        if (this.weatherSub) {
            this.weatherSub.unsubscribe();
        }

        const months: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Now', 'Dec'];

        let categories: string[] = [], data: number[] = [];

        this.weatherSub = MeteorObservable.subscribe('weather').subscribe(() => {

            this.zone.run(() => {
                (WeatherCollection.find)({}, {sort: {month_id: 1}}).fetch().forEach((value) => {
                    categories.push(months[value.month_id - 1]);
                    data.push(value.value);
                });
                console.log(categories, data);
                this.chart_options = {
                    xAxis: {
                        categories: categories
                    },
                    title: {text: 'Средняя температура за год в городе Барселона'},
                    series: [{
                        name: "Температура воздуха",
                        data: data,
                    }]
                };
            });
        });
    }

    ngOnInit(): void {
        console.log('[AdminChartComponent:ngOnInit]');
    }

    ngOnDestroy(): void {
        this.weatherSub.unsubscribe();
    }

    logout1(): boolean {
        console.log('[AdminChartComponent:logout]');
        Meteor.logout((err) => {
            this.zone.run(() => {
                if (err) {
                    console.log('[SignupComponent:logout] error', err);
                } else {
                    console.log('[SignupComponent:logout] success');
                    this.router.navigate(['/']);
                }
            });
        });
        return false;
    }


    chart_options: Object;
}