import {NgModule}      from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule}      from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {ChartModule} from 'angular2-highcharts';

import {AppComponent}  from './app.component';
import {routes, ROUTES_PROVIDERS} from "./app.routes";

import {AUTH_DECLARATIONS} from "./auth/index";
import {ADMIN_DECLARATIONS} from "./admin/index";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        RouterModule.forRoot(routes),
        ChartModule.forRoot(require('highcharts'))
    ],
    declarations: [
        AppComponent,
        ...AUTH_DECLARATIONS,
        ...ADMIN_DECLARATIONS
    ],
    providers: [
        ...ROUTES_PROVIDERS
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}