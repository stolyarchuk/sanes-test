import {Component, NgZone, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import template from './login.component.html';
import style from './auth.scss';

@Component({
    selector: 'login',
    template,
    styles: [style],
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    private error: string;

    constructor(private router: Router, private zone: NgZone, private formBuilder: FormBuilder) {

    }

    ngOnInit(): void {
        if (Meteor.userId()) {
            console.log('[LoginComponent:ngOnInit] logged');
            this.router.navigate(['/admin']);
        }
        console.log('[LoginComponent:ngOnInit]');
        this.loginForm = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });

        this.error = '';
    }

    login() {
        if (!this.loginForm.valid) {
            return;
        }
        Meteor.loginWithPassword(this.loginForm.value.email, this.loginForm.value.password,
            (err) => {
                this.zone.run(() => {
                    if (err) {
                        console.log('[LoginComponent:login] error', err);
                        this.error = err.message;
                    } else {
                        console.log('[LoginComponent:login] success');
                        this.router.navigate(['/admin']);
                    }
                });
            });

    }


}