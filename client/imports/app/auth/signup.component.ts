import {Component, NgZone, OnInit} from '@angular/core';
import {Accounts} from 'meteor/accounts-base';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import template from './signup.component.html';
import style from './auth.scss';

@Component({
    selector: 'signup',
    template,
    styles: [style]
})
export class SignupComponent implements OnInit {
    signupForm: FormGroup;
    private error: string;

    constructor(private router: Router,
                private formBuilder: FormBuilder,
                private zone: NgZone) {
    }

    signup(): void {
        if (!this.signupForm.valid) {
            console.log('[SignupComponent:signup] form invalid', this.signupForm);
            return;
        }
        if (this.signupForm.value.password !== this.signupForm.value.confirm_password) {
            console.log('[SignupComponent:signup] form invalid. Passwords mismatch', this.signupForm.value.password, this.signupForm.value.confirm_password);
            this.error = 'Passwords mismatch';
            return;
        }
        console.log('[SignupComponent:signup] valid', this.signupForm.value);
        Accounts.createUser({
            username: this.signupForm.value.username,
            email: this.signupForm.value.email,
            password: this.signupForm.value.password
        }, (err) => {
            if (err) {
                this.zone.run(() => {
                    console.log('[SignupComponent:signup] error', err);
                    this.error = err.message;
                });
            } else {
                this.error = '';
                console.log('[SignupComponent:signup] registered');
                this.router.navigate(['/admin']);
            }
        });


    }

    ngOnInit(): void {
        console.log('[SignupComponent:ngOnInit]');
        if (Meteor.userId()) {
            console.log('[SignupComponent:ngOnInit] logged');
            this.router.navigate(['/admin']);
        }
        this.signupForm = this.formBuilder.group({
            username: ['', Validators.required],
            email: ['', Validators.required],
            password: ['', Validators.required],
            confirm_password: ['', Validators.required]
        });

        this.error = '';
    }

}